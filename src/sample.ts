﻿///
//        repeat x  Find repeat statemements   /(repeat [0-9]*)/igm
//
//  
//          // gets as many instances as needed
//          /(repeat *([0-9]*).*)\[(.*)\]*/igm
//
//
// statement number
// valid statements: up down left right 
// repeat [ statement number ]

enum Type {
    Number,
    String
}

interface Token {
    value: any;
    type: Type;
}

interface Statement {
    command: string;
    number: number;
}

let editor: any;

function parse(): void {

    var code = editor.model.toRawText().lines.join(" ") as string;

    code = code.toLowerCase();

    let functions = parseRepeat(code);

    // run the instructions
    for (let fn of functions) {
        fn();
    }

}

function parseRepeat(code: string): Function[] {

    var matches = /(repeat *([0-9]*).*)\[(.*)\]*/igm.exec(code);

    var functions = [];

    var count = matches[2];

    var body = matches[3];

    var tokens = tokenizeStatements(body);

    var functs = parseStatements(tokens);

    functions.push(function () {
        repeat(count, functs);
    });

    return functions;

}

/**
 * 
 * @param c
 */
function isNumeric(c: string) {

    let charCode = c.charCodeAt(0);

    if (charCode >= 48 && charCode <= 57) {
        return true;
    }

    return false;

}

/**
 * Tokenizes a statement within a repeat [ stmt ]
 * 
 * @param code
 */
function tokenizeStatements(code: string): Token[] {

    //var matches = /((up|right|down|left).([0-9]+))+/igm.exec(code);

    let tokens: Token[] = [];
    let token = '';
    let currentType: Type = null;

    // loop over each character and extract the tokens
    for (let c of code) {

        if (c == ' ') {

            if (token) {

                tokens.push({
                    value: token,
                    type: currentType
                });

                token = '';
            }

        } else {

            if (isNumeric(c)) {
                currentType = Type.Number;
            } else {
                currentType = Type.String;
            }

            token += c; // build this token

        }

    }

    return tokens;

}


function parseStatements(tokens: Token[]): Function[] {

    let fns: Function[] = [];

    console.log("parsing tokens: ", tokens);

    for (let i = 1; i < tokens.length; i += 2) {

        let t1 = tokens[i - 1];
        let t2 = tokens[i];

        let fn = null;

        if (t1.value === "up") {

            fn = function () {
                up(t2.value);
            }

        } else if (t1.value === "down") {

            fn = function () {
                down(t2.value);
            }

        } else if (t1.value === "right") {

            fn = function () {
                right(t2.value);
            }

        } else if (t1.value === "left") {

            fn = function () {
                left(t2.value);
            }

        }

        fns.push(fn);

    }

    return fns;

}
// Execution Actions

interface IEmitter {
    repeat(count, functions: Function[]); // This takes a function[] because the parser itself is written in JS
    up(count: number): void;
    down(count: number): void;
    left(count: number): void;
    right(count: number): void;

}

/**
 * Emits JS 
 */
class JSEmitter implements IEmitter {

    repeat(count, fns) {

        console.log("repeat " + count);

        for (var i = 0; i < count; i++) {

            console.log("[");

            for (let f of fns) {
                f();
            }

            console.log("]");

        }

    }

    up(count) {
        console.log("up( " + count + ")");
    }

    down(count) {
        console.log("down " + count);
    }

    right(count) {
        console.log("right " + count);
    }

    left(count) {
        console.log("left " + count);
    }

}

class JSRuntime {

    repeat(count, fns) {

        for (var i = 0; i < count; i++) {

            for (let f of fns) {
                f();
            }

        }

    }

    up(count) {
        // move marker UP by count
    }

    down(count) {
        // move marker down by count
    }

    right(count) {
        // move marker RIGHT by count
    }

    left(count) {
        // move marker LEFT by count
    }
}


function repeat(count, fns) {

    console.log("repeat " + count);

    for (var i = 0; i < count; i++) {

        console.log("[");

        for (let f of fns) {
            f();
        }

        console.log("]");

    }

}

function up(count) {
    console.log("up " + count);
}

function down(count) {
    console.log("down " + count);
}

function right(count) {
    console.log("right " + count);
}

function left(count) {
    console.log("left " + count);
}


document.addEventListener("DOMContentLoaded", function (event) {

    document.getElementById('btnParse').addEventListener('click', parse);
});

document.onload = function () {

}
